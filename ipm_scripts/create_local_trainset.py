import os
from tqdm import tqdm
import dtlpy as dl
from shefi_sandbox.shefi_logging import get_script_logger

logger = get_script_logger('IPM-local_prepare')

def list_boxes(item: dl.entities.Item):
    """Returns a list of the boxes shape"""
    from collections import namedtuple
    Box = namedtuple('Box', ['width', 'height', 'left', 'top'])

    box_anns = [ann for ann in item.annotations.list() if ann.type == 'box']
    box_list = [Box(ann.x[1]-ann.x[0], ann.y[1]-ann.y[0], ann.x[0], ann.y[0]) for ann in box_anns]
    return box_list

def parse_yolo_annotation_single_item(item: dl.entities.Item, label_map, base_path):
    """
    Return yolo3 annotation format
        filepath x_min,y_min,x_max,y_max,class_id (no space).
    :param item:
    :param label_map:
    :return:
    """
    box_anns = [ann for ann in item.annotations.list() if ann.type == 'box']
    ann_format = f'{base_path}/items{item.filename}' # filename starts with '/'
    # Box format: x_min,y_min,x_max,y_max,class_id (no space).
    for ann in box_anns:
        x_min, y_min = int(ann.x[0]), int(ann.y[0])
        x_max, y_max = int(ann.x[1]), int(ann.y[1])
        c_id = label_map.get(ann.label, 0) # if ann.label not found ==> defaults to index 0
        ann_format += f" {x_min},{y_min},{x_max},{y_max},{c_id}"

    ann_format += "\n"
    return ann_format


def prepare_yolo3_train_data(dataset_name, out_dir='/tmp', label_map=None):
    """
    Download the data from dlp and create the gt vectors of the labels
    """
    # TODO: split train-val - Currently part of the test function
    import os
    import datetime
    project = dl.projects.get('IPM Glue Trap')
    dataset = project.datasets.get(dataset_name)
    dataset_path = f"{out_dir}/yolo3-{datetime.datetime.now().strftime('%F')}"
    annotation_path = f"{dataset_path}/train.txt"
    os.makedirs(dataset_path, exist_ok=True)
    if label_map is None:
        label_map = dataset.instance_map  # `name` : `id`: int
        label_map = {k: v-1 for k, v in label_map.items()}

    filters = dl.Filters(field='annotated', values=True)
    filters.add(field='annotated', values='discarded', operator='ne')  # filter out discarded items
    filters.add_join(field="type", values="box")  # make sure only items w/ box annotations

    # Download the DQL related to train
    dataset.items.download(filters=filters, local_path=dataset_path, annotation_options='json')

    with open(f"{dataset_path}/ipm_classes.txt", 'w') as f:
        for cls in label_map.keys():
            f.write(f"{cls}\n")

    # create a new yolo3 annotation file
    with open(annotation_path, 'w') as f:
        pass

    cnt = 0
    annotations_count = 0
    boxes_list = []

    pages = dataset.items.list(filters=filters)
    for page in tqdm(pages, position=0, desc="page", ncols=100, total=pages.total_pages_count):  # dynamic_ncols=
        for item in tqdm(page, position=0, leave=True, ncols=80):
            tmp_box_list = list_boxes(item)
            annotations_count += len(tmp_box_list)
            boxes_list += tmp_box_list
            cnt += item.annotations_count  # FIXME - this includes the complete and discard
            with open(annotation_path, 'a') as f:
                item_anns_fmt = parse_yolo_annotation_single_item(item, label_map=label_map, base_path=dataset_path)
                f.write(item_anns_fmt)

    # Anchors suggestion:
    import numpy as np
    from sklearn.cluster import KMeans
    kmeans = KMeans(init='random', n_clusters=9, n_init=5, )
    kmeans.fit(np.array([[box.width, box.height] for box in boxes_list]))
    anchors = kmeans.cluster_centers_
    logger.debug(f"Anchors calculated using K-means:\n {anchors}")


    # TODO: add anchors and base model.h5 model
    # TODO: add config file


    logger.info(f"""Created new dataset for train @{datetime.datetime.now().strftime('%X %F')} - total boxes {annotations_count} ({cnt})
            data was saved at {dataset_path}
          :::  create the model object with DQL, ids and name""")


if __name__ == '__main__':
    import argparse
    import json
    parser = argparse.ArgumentParser(
        description='ipm insects model creation and interface',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--dataset', default=False, action='store', help='name of dataset to prepare')
    parser.add_argument('--label-map', nargs='*', help='pass the label map as a list of names')
    parser.add_argument('--model-name', default='yolo3', help='which model to prepare the data for')

    args = parser.parse_args()

    if args.label_map is not None:
        label_map = {key: idx for key, idx in zip(args.label_map, range(len(args.label_map))) }
        confirm = input(f"Please confirm label by pressing (y/[n])\n{json.dumps(label_map, indent=2)}\n")
        if confirm.lower() in ['yes', 'y']:
            pass
        else:
            raise InterruptedError("Closing.\n\tplease select a valid label map. `--label-map` <key_0> <key_1> ...`")

    if args.dataset:
        prepare_yolo3_train_data(dataset_name=args.dataset, label_map=label_map)

