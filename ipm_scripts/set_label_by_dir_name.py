# for the septber batch we started by annotating 'insect genric'
# this script should convert them to the specific class

import dtlpy as dl
import os
project = dl.projects.get('IPM Glue Trap')
dataset = project.datasets.get('base-insects-sep-20')

filters = dl.Filters()
# filters.add(field='complete', values=True)
#filters.add(field='metadata.system.annotationStatus', values='completed')
filters.add(field='annotated', values='discarded', operator='ne')
pages = dataset.items.list(filters=filters)

for page in pages:
    for item in page:
        label_name_from_dir = item.filename.split('/')[1]
        for ann in item.annotations.list():
            if ann.type != 'box':
                continue
            else:
                # print(f"change {item.name} ann: {ann.label} => {label_name_from_dir}")
                ann.label = label_name_from_dir
                ann.update()
        print(f"Updateing {item.filename}")
        #item.annotations.update()



# ann_filter = dl.Filters()
# ann_filter.resource = dl.FiltersResource.ANNOTATION
# ann_filter.add(field='type', values='box')
# ann_filter.add_join()