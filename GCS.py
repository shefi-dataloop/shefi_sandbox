from google.cloud import storage
from shefi_sandbox.shefi_logging import get_script_logger, set_stream_handler

class GCS():

    def __init__(self):
        self.client = storage.Client(project='viewo')
        self.logger = get_script_logger('GCS')

    @property
    def get_default_bucket(self):
        bucket = self.client.get_bucket('shefi-misc-bucket')
        return bucket

    def upload(self, local_path, remote_path, bucket=None):
        """Uploads a local file to remote path on bloc; return the public url"""
        if bucket is None:
            bucket = self.get_default_bucket
        blob = bucket.blob(remote_path)
        blob.upload_from_filename(local_path)

        return blob.public_url

