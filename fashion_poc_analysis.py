import dtlpy as dl
from tqdm import tqdm
import numpy as np
import pandas as pd
from sklearn import metrics


LABEL_MAP = {0: "T-shirt/Top", 1: "Trouser", 2: "Pullover", 3:  "Dress", 4: "Coat",
             5: "Sandals", 6: "Shirt", 7: "Sneaker", 8: "Bag", 9: "Ankle Boots"}

def get_task_acc(ds: dl.entities.dataset, ref_ds: dl.entities.dataset):
    page_size = 256
    filters = dl.Filters(field='annotated', values=True)
    filters.add(field='metadata.system.annotationStatus', values='completed')
    pages = ds.items.list(filters=filters, page_size=page_size)

    tot = pages.items_count
    false_list = []
    discard_list = []
    labels_dict = {}

    for page in tqdm(pages, desc="pages", ncols=100, total=int(tot/page_size)):
        for item in page:
            ref_filename = f"/mnist-test/{item.name}"
            ref_item = ref_ds.items.get(ref_filename)
            anns = [ann.label for ann in item.annotations.list() if ann.label not in ['completed', 'approved', 'discarded']]
            ref_anns = [LABEL_MAP[int(ann.label)] for ann in ref_item.annotations.list()]

            if anns != ref_anns:
                print(f"\nitem {item.name} ({item.id}) differ from ref item {ref_item.name}:\n\t{anns}:{ref_anns}")
                if anns == []:
                    discard_list.append(item)
                    print(f"item {item.name} has no valid annotation left")
                    continue
                else:
                    false_list.append(item)

            # TODO: i saw occosions w/ two marked labels.... - don't ref as item [0] ?!?
            single_entry = {'name': item.name, 'item': item, 'ref_ann': ref_anns[0], 'ann': anns[0]}
            labels_dict[item.id] = single_entry

    accuracy = 1 - (len(false_list) / tot)
    print(f"Total annotated items in {ds.name} = {tot}\n",
          f"total false = {len(false_list)}",
          f"accuracy = {accuracy}")
    # item.print()
    # ref_item.print()

    return labels_dict

def show_confusion_mat(labels_dict, plot_confusion=False):
    prd, act  = [], []
    for item_id, it in labels_dict.items():
        prd.append(it['ann'])
        act.append(it['ref_ann'])

    label_names = list(LABEL_MAP.values())
    cm = metrics.confusion_matrix(y_true=act, y_pred=prd, labels=label_names)
    # rows are the actuate,
    # cols are the predicted
    df = pd.DataFrame(cm, columns=label_names, index=label_names)
    df['tot_act'] = df.apply(sum, axis=1)

    if plot_confusion:
        from sklearn.metrics._plot.confusion_matrix import ConfusionMatrixDisplay
        from matplotlib import pyplot as plt

        disp = ConfusionMatrixDisplay(confusion_matrix=cm,
                                      display_labels=list(LABEL_MAP.values()))
        disp.plot(include_values=True,
                  cmap='viridis', ax=None, xticks_rotation='horizontal',
                  values_format=None)
        plt.show()

    return df

if __name__ == '__main__':
    dl.setenv('prod')
    project = dl.projects.get('shefi-contests')
    ref_ds = project.datasets.get('fashion-mnist')
    clear_ds = project.datasets.get('fashion-mnist-poc-clear')
    sugg_ds = project.datasets.get('fashion-mnist-poc-suggestion')

    print(f"clear items results")
    label_dict_clear = get_task_acc(clear_ds, ref_ds)
    print(f"pre-suggested items results")
    label_dict_sugg = get_task_acc(sugg_ds, ref_ds)


