from tensorflow.keras.datasets import fashion_mnist, mnist
from matplotlib import pyplot as plt
import numpy as np
from sklearn.manifold import TSNE
from time import time,sleep

from matplotlib import offsetbox
from sklearn import (manifold, datasets, decomposition, ensemble,
                     discriminant_analysis, random_projection, neighbors)

# fashion mnist
(train_images, train_labels), (test_images, test_labels) = fashion_mnist.load_data()
class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat', 'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']
# flatten the data to 1d features
X_train = train_images.reshape(train_images.shape[0], -1)

def show_sample():
    plt.figure(figsize=(10, 10))
    for i in range(25):
        plt.subplot(5, 5, i+1)
        plt.xticks([])
        plt.yticks([])
        plt.grid(False)
        plt.imshow(train_images[i], cmap=plt.cm.binary)
        plt.xlabel(class_names[train_labels[i]])
    plt.show()


# from https://scikit-learn.org/stable/auto_examples/manifold/plot_lle_digits.html#sphx-glr-auto-examples-manifold-plot-lle-digits-py
# ----------------------------------------------------------------------
# Scale and visualize the embedding vectors
def plot_embedding(X, y, thr=4e-3, title=None):
    """
    Embeding visualization
    :param X: train
    :param y: labels
    :param thr: thumbnail thr to visuzlize the items
    :param title: title to the image
    :return:
    """
    x_min, x_max = np.min(X, 0), np.max(X, 0)
    X = (X - x_min) / (x_max - x_min)

    plt.figure()
    ax = plt.subplot(111)
    for i in range(X.shape[0]):
        plt.text(X[i, 0], X[i, 1], str(y[i]),
                 color=plt.cm.Set1(y[i] / 10.),
                 fontdict={'weight': 'bold', 'size': 9})

    if hasattr(offsetbox, 'AnnotationBbox'):
        # only print thumbnails with matplotlib > 1.0
        shown_images = np.array([[1., 1.]])  # just something big
        for i in range(X.shape[0]):
            dist = np.sum((X[i] - shown_images) ** 2, 1)
            if np.min(dist) < thr:
                # don't show points that are too close
                continue
            shown_images = np.r_[shown_images, [X[i]]]
            imagebox = offsetbox.AnnotationBbox(
                offsetbox.OffsetImage(train_images[i], cmap=plt.cm.gray_r),
                X[i])
            ax.add_artist(imagebox)
    plt.xticks([]), plt.yticks([])
    if title is not None:
        plt.title(title)


max_points = 500
X = X_train[:max_points,:]
y = train_labels[:max_points]
print(f"using only {max_points} for the embedding")
# ----------------------------------------------------------------------
# t-SNE embedding of the digits dataset
print("Computing t-SNE embedding")
tsne = manifold.TSNE(n_components=2)#, init='pca', random_state=0)
tic = time()
X_tsne = tsne.fit_transform(X)
toc = time()

plot_embedding(X_tsne,
               y,
               thr=1e-2,
               title="t-SNE embedding of the mnist_fashion (time {t:.2f}s)".format(t=tic-toc)
               )

# ----------------------------------------------------------------------
print("Computing Spectral embedding")
embedder = manifold.SpectralEmbedding(n_components=2, random_state=0, eigen_solver="arpack")
t0 = time()
X_se = embedder.fit_transform(X)
toc = time()

plot_embedding(X_se,
               y,
               thr=1e-2,
               title="Spectral embedding of the mnist_fashion (time {t:.2f}s)".format(t=tic-toc)
    )

plt.show()
