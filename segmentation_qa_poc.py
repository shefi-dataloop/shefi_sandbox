import dtlpy as dl
import numpy as np
import pandas as pd
from shefi_sandbox import faiss_poc
from collections import namedtuple
from shefi_sandbox.shefi_logging import get_script_logger
logger = get_script_logger('QC-segmentation', to_file=True)

Source = namedtuple('Source', ['project', 'dataset'])
SOURCES = {
    'Elbit':
        [Source('Elbit', 'June_19'),
         Source('Elbit', 'July_19'),
         Source('Elbit', '11_17_2019'),
         Source('Elbit', '20_11_2019'),
         Source('Elbit', '17_12_2019'),
         Source('Elbit', 'ben_shemen_13_02_2020')],
    'Elbit-short':
        [Source('Elbit', 'ben_shemen_13_02_2020')]  # 11_17_2019  # ben_shemen_13_02_2020
}
DEBUG=False

def get_label_name_converter(source_name):
    if source_name == 'Elbit':
        return lambda lbl_name: str.replace(lbl_name, ' ', '_').lower()
    else:
        # Idle converter
        return lambda x: x


def get_label_map(source_name):
    """ Return the label map (key=id, val=name)
        and the inverse map (key=name, val=id)
        by adding all label in the dataset
        if needed aggregates over several dataset
    """
    dl.setenv('prod')
    label_map, inv_label_map = {}, {}
    lbl_id = 0
    for src in SOURCES[source_name]:
        project = dl.projects.get(src.project)
        dataset = project.datasets.get(src.dataset)
        for label in dataset.labels:
            # label = label.tag.replace(' ', '_').lower()  # convert the label to lower case with no spaces
            converter = get_label_name_converter(source_name)
            label = converter(label.tag)

            if label not in inv_label_map:
                label_map[lbl_id] = label
                inv_label_map[label] = lbl_id
                lbl_id += 1

    return label_map, inv_label_map


def get_datasets_to_df(source_name='Elbit'):
    """
    Loads multiple dataset to one df
    :param source_name: `str` as defined in SOURCES
    :return: features df and inv_label_mapping (from name to id)
    """
    # Init params for all datasets
    df = None
    label_map, inv_label_map = get_label_map(source_name)
    model = faiss_poc.get_model(model_name='resnet')

    dl.setenv('prod')
    for src in SOURCES[source_name]:
        project = dl.projects.get(src.project)
        dataset = project.datasets.get(src.dataset)
        logger.info(f"dataset {dataset.name} has {dataset.itemsCount} items")

        temp_df = faiss_poc.prepare_features_df_dlp(model, dataset, inv_label_map, features_df=None, filter=None,
                                                    label_converter=get_label_name_converter(source_name))
        temp_df.insert(1, 'ds_name', dataset.name)  # add dataset name after the item_id col

        if df is None:
            df = temp_df
        else:
            df = pd.concat([df, temp_df], ignore_index=True)
    return df, inv_label_map


def get_possible_errors(index_df: pd.DataFrame, queries_df: pd.DataFrame, sim_thr=0.6):
    """
    create a faiss index and preforms the query.
    the function than get the possible error list and returns a DataFrame containing those items w/ possible errors
    with additional field to measure the dissimilarity

    :param index_df: `pd.DataFrame` of items and their features, that will build the search index
    :param queries_df:  `pd.DataFrame` of items to be queried on the index
    :param sim_thr: `float` [0-1], items with lower similarity (label agreement) will be treated as errors
    :return: `pd.DataFrame` of possible errors (from the queries_df)
    """

    model = faiss_poc.get_model(model_name='resnet')
    index = faiss_poc.load_index(feature_df=index_df, model=model)
    # NOTE: query from df to same df -> ignore first nn
    query_returned_labels, I, D = faiss_poc.query_index(index, queries_df=queries_df, index_df=index_df, multi_label=True, return_search=True)
    # remove the return result for the same query
    query_returned_labels = query_returned_labels[:, 1:]
    I, D = I[:, 1:], D[:, 1:]
    # index_df.item_id.iloc[I[i, j]]

    faiss_poc.calc_accuracies(queries_df=queries_df, query_returned_labels=query_returned_labels, multi_label=True)
    error_df = faiss_poc.create_error_df(queries_df=queries_df, query_returned_labels=query_returned_labels)
    logger.debug(error_df.head(3))

    errors_arr = debug_acc_calcs(queries_df=queries_df, query_returned_labels=query_returned_labels, return_array=True)
    error_indices = errors_arr < sim_thr
    dist_error_df = faiss_poc.create_error_df(queries_df=queries_df, query_returned_labels=query_returned_labels,
                                              error_indices=error_indices)
    dist_error_df['similarity_acc'] = errors_arr[error_indices]
    dist_error_df['nn_item_id'] = index_df.itemId.iloc[I[error_indices, 0]].to_list()  # to_list to avoid the samoe column name
    logger.info(f"Created Error dataframe from similarity (thr={sim_thr}). number of error {np.sum(error_indices)} out of {len(error_indices)}")
    return dist_error_df


def debug_acc_calcs(queries_df: pd.DataFrame, query_returned_labels: np.ndarray, return_array=False):
    return_labels_type = query_returned_labels.dtype
    nof_queries, knn = query_returned_labels.shape

    act = queries_df.label_id.to_numpy().astype(return_labels_type)
    prd_1nn = query_returned_labels[:, 1]  # query was done to index containing all queries as well
    acc_nor_tot_arr = faiss_poc.multi_label_dist_accuracy(act_labels=act, prd_labels=prd_1nn, method='len', normalize='total', return_list=True)
    acc_nor_act_arr = faiss_poc.multi_label_dist_accuracy(act_labels=act, prd_labels=prd_1nn, method='len', normalize='act', return_list=True)

    acc_nor_tot = np.mean(acc_nor_tot_arr)
    acc_nor_act = np.mean(acc_nor_act_arr)

    # TODO: add some statistics for the score of each item (DataFrame or maybe histogram)
    nof_thr_items = {}  # number of items with similarity score under certain thr
    for thr in [0.5, 0.6, 0.7, 0.8, 0.9]:
        nof_thr_items[thr] = {
            'tot': np.sum(acc_nor_tot_arr < thr),
            'act': np.sum(acc_nor_act_arr < thr)
        }

    logger.info(f"""distance accuracies debug:
    normalize by total : {acc_nor_tot*100:2.1f}%  (90-{np.percentile(acc_nor_tot_arr, 90)}:10-{np.percentile(acc_nor_tot_arr, 10)}   percentiles) 
        (T<{0.8} {nof_thr_items[0.8]['tot'] / nof_queries:.2f}  #{nof_thr_items[0.8]['tot']}) 
        (T<{0.7} {nof_thr_items[0.7]['tot'] / nof_queries:.2f}  #{nof_thr_items[0.7]['tot']})
        (T<{0.6} {nof_thr_items[0.6]['tot'] / nof_queries:.2f}  #{nof_thr_items[0.6]['tot']})
        (T<{0.5} {nof_thr_items[0.5]['tot'] / nof_queries:.2f}  #{nof_thr_items[0.5]['tot']})
    normalize by actual: {acc_nor_act*100:2.1f}%  
        (T<{0.8} {nof_thr_items[0.8]['act'] / nof_queries:.2f}  #{nof_thr_items[0.8]['act']}) 
        (T<{0.7} {nof_thr_items[0.7]['act'] / nof_queries:.2f}  #{nof_thr_items[0.7]['act']})
    """)
    if return_array:
        return acc_nor_tot_arr


def get_files_path(dump_dir='/tmp', source='Elbit', data_ver=0):
    """
    Creates and return the path to the local files
    :returns: df_path, label_map_path
    """
    df_dump_path = f"{dump_dir}/features-{source}-v{data_ver}-resnet.pickle"
    label_map_dump_path = f"{dump_dir}/features-{source}-v{data_ver}-resnet.json"
    return df_dump_path, label_map_dump_path


def dump_files(df, inv_label_mapping, dump_dir='/tmp', source='Elbit', data_ver=0):
    """
    Saves all the data to disk.
    :param df: `pandas.DataFrame` containing all items feature representation
    :param inv_label_mapping: `dict` w/ key=name; val=id of all used labels
    :param dump_dir: local FS path to dump the data
    :param source: `str` to be used as identifier when saving the data
    :param data_ver: `int` or 'str' to be used after the 'V' as identifier
    :returns: nothing, only operates on local FileSystem
    """
    import pickle, json
    df_dump_path, label_map_dump_path = get_files_path(dump_dir=dump_dir, source=source, data_ver=data_ver)
    with open(df_dump_path, 'wb') as f:
        pickle.dump(df, f)
    with open(label_map_dump_path, 'w') as f:
        json.dump(inv_label_mapping, f)
    logger.info(f"saved all files. df path {df_dump_path}")


def load_files(load_dir='/tmp', source='Elbit', data_ver=0):
    import pickle, json
    df_load_path, label_map_load_path = get_files_path(dump_dir=load_dir, source=source, data_ver=data_ver)
    with open(df_load_path, 'rb') as f:
        df = pickle.load(f)
    with open(label_map_load_path, 'r') as f:
        inv_label_mapping = json.load(f)
    logger.info(f"loaded all files. df path {df_load_path}")
    return df, inv_label_mapping


def create_html_page(error_df: pd.DataFrame, inv_label_map: dict, description=None):
    import time
    tic = time.time()
    error_html = faiss_poc.error_df2html_not_fake(error_df=error_df, inv_map=inv_label_map, return_as_str=True)
    toc = time.time()
    logger.debug(f"execution of 'error_df2html_not_fake' took {toc-tic}")

    labels_df = pd.DataFrame(inv_label_map.values(), index=inv_label_map.keys(), columns=['label_id'])
    label_map_html = labels_df.transpose().to_html(escape=False)

    logger.info(f"creating segmentation error report. df shape is {error_df.shape}")
    with open('/tmp/segmentation-error.html', 'w') as f:
        if __name__ == '__main__':
            f.write(f'<h2>{args.source} error table</h2> <p>{description}</p> <hr>')
        f.write(label_map_html)
        f.write('<hr><br>')
        f.write(error_html)

    # TEST JINJA TEMPLATES
    logger.debug("strating Jinja templating")
    from jinja2 import Template
    template = Template(open('qc-report.html', 'r').read())
    rendered = template.render(source_name=args.source,
                               desc=description,
                               label_df=labels_df.transpose(),
                               error_df_html=error_html)
    with open('/tmp/segmentation-err-from-template.html', 'w') as f:
        f.write(rendered)

def main(source: str, load=False, dump_dir='/tmp', data_ver=0, sim_thr=0.5):
    if load:
        df, inv_label_mapping = load_files(load_dir=dump_dir, source=source, data_ver=data_ver)
    else:
        df, inv_label_mapping = get_datasets_to_df(source_name=source)
        dump_files(df, inv_label_mapping, dump_dir=dump_dir, source=source, data_ver=data_ver)

    logger.debug(f"random sample of df:\n{'='*100}\n{df.sample(n=10)}")

    error_df = get_possible_errors(index_df=df, queries_df=df, sim_thr=sim_thr)
    # create_html_page(error_df, inv_label_map=inv_label_mapping, description=f"source={source}, with similarity thr: {sim_thr}")
    faiss_poc.create_error_report(error_df=error_df, inv_map=inv_label_mapping, dataset=None, out_dir=dump_dir,
                                  output_columns=['item_link', 'label_name', 'predict_names', 'nn_item_id'], source=source,
                                  desc=f"source={source}, with similarity thr: {sim_thr}")
    logger.info(f"Main completed!          ARGS: {args}")


if __name__ == '__main__':
    import argparse
    import os
    parser = argparse.ArgumentParser(description='preforms multi-label Nearest Neighbour search to assist QC',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--source', default='Elbit-short', choices=list(SOURCES.keys()), help='Dataloop source data')
    parser.add_argument('--data-ver', default=0, help='identifier to the dump dir')
    parser.add_argument('--dump-dir', default='/tmp',  help='path to the dump dir')
    parser.add_argument('--load', default=False, action='store_true',
                        help='If passed does not creates the data and skips directly to the error calculations')
    parser.add_argument('--debug', default=False, action='store_true', help='If passed adds debug logging')
    args = parser.parse_args()
    if args.debug:
        from shefi_sandbox.shefi_logging import set_stream_handler
        set_stream_handler(logger, level='DEBUG')
        set_stream_handler(faiss_poc.logger, level='DEBUG')


    main(source=args.source, load=args.load, dump_dir=os.path.expanduser(args.dump_dir), data_ver=args.data_ver)