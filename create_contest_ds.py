import dtlpy as dl
import pickle
import numpy as np
import os
import time
import cv2
from tqdm import tqdm
import random
from multiprocessing.pool import ThreadPool as Pool

r_color = lambda:  "#{:06x}".format(random.randint(0, 0xFFFFFF))

DLP_CFG = {
    'prod': {
        'project': {'name': 'shefi-contests',
                    'id': '50f0fc03-4d70-455d-b485-c78cca53f2be'},
        'dataset': {'name': 'ttt',
                    'id': 'n/a'}
    },
    'dev': {
        'project': {'name': 'Shefi_onboard',
                    'id': '3d9fd1b6-be52-4fae-b4c6-07b66b549df5'},
        'dataset': {'name': 'shefi-competiions-ds',
                    'id': '5f2294a49c94b7524486e50f'}
    }
}

SOURCES = {
    'cifar-10': os.path.join(os.getenv('HOME'), 'contests_ds/cifar-10-batches-py'),
    'cifar-100': os.path.join(os.getenv('HOME'), 'contests_ds/cifar-100-python'),
    'mnist': os.path.join(os.getenv('HOME'), 'contests_ds/pascal_voc'),
    'fashion-mnist': '',
    'pascal-voc': os.path.join(os.getenv('HOME'), 'contests_ds/pascal-voc'),
    'ms-coco': '', # Used default in ~/.mxnet/...
    'ms-coco2': '',  # Used default in ~/.mxnet/...
}

def cifar_10_extract_batch(batch_path, labels, remote_base='/'):
    """
    extract a single batch of cifar images and save them to dlp platform
    """
    tmp_local_path = '/tmp/sgl_img.png'
    with open(batch_path, 'rb') as f:
        batch = pickle.load(f, encoding='bytes')

    for i, lbl_id in tqdm(enumerate(batch[b'labels'])):
        img = batch[b'data'][i].reshape(3, 32, 32)
        img_rgb = np.moveaxis(img, 0, -1)  # push color channel to last dim

        # save locally and upload to dlp
        cv2.imwrite(tmp_local_path, img_rgb)
        remote_path = os.path.join(remote_base, batch[b'filenames'][i].decode('utf-8'))

        #print(f"local = {tmp_local_path}, remote_base = {remote_base}, remote_full={remote_path}")

        item = dataset.items.upload(local_path=tmp_local_path, remote_name=remote_path)
        # item.print()

        # add classification annotation
        builder = item.annotations.builder()
        builder.add(annotation_definition=dl.Classification(label=labels[lbl_id]))
        anns = item.annotations.upload(builder)


def cifar10_extract():
    with open(os.path.join(source_dir_path, 'batches.meta'), 'rb') as f:
        meta = pickle.load(f, encoding='bytes')

    labels = [l.decode('utf-8') for l in meta[b'label_names']]
    base_dir = os.path.basename(source_dir_path)
    print(f"num of items in batch {meta[b'num_cases_per_batch']}\n labels={labels}")

    try:
        dataset.add_labels(labels)
    except dl.exceptions.InternalServerError:
        print("did not add labels to dataset {}".format(dataset.name))

    # Train-data
    if not args.skip == 'train':
        for nof_batch in range(1,5):
            print(f"starting {nof_batch} ")
            batch_path = os.path.join(source_dir_path, f"data_batch_{nof_batch}")
            cifar_10_extract_batch(batch_path=batch_path, labels=labels, remote_base=base_dir+'-train')
            print(f"batch {nof_batch} extracted")


    # Test-data
    if not args.skip == 'val':
        test_batch_path = os.path.join(source_dir_path, "test_batch")
        cifar_10_extract_batch(batch_path=test_batch_path, labels=labels, remote_base=base_dir+'-test')


def cifar_100_extract_batch(batch_path, labels, remote_base='/'):
    """
    extract a single batch of cifar images and save them to dlp platform
    in cifar 100 there is two level of labels - coarse + fine
    """
    tmp_local_path = '/tmp/sgl_img.png'
    with open(batch_path, 'rb') as f:
        batch = pickle.load(f, encoding='bytes')

    for i, (lbl_id_coarse, lbl_id_fine) in tqdm(enumerate(zip(batch[b'coarse_labels'], batch[b'fine_labels']))):
        img = batch[b'data'][i].reshape(3, 32, 32)
        img_bgr = np.moveaxis(img, 0, -1)  # push color channel to last dim

        # save locally and upload to dlp
        cv2.imwrite(tmp_local_path, img_bgr[:,:, ::-1])
        remote_path = os.path.join(remote_base, batch[b'filenames'][i].decode('utf-8'))

        item = dataset.items.upload(local_path=tmp_local_path, remote_name=remote_path)
        # add classification annotation
        builder = item.annotations.builder()
        item_label = "{coarse}.{fine}".format(coarse=labels['coarse'][lbl_id_coarse], fine=labels['fine'][lbl_id_fine])
        builder.add(annotation_definition=dl.Classification(label=item_label))
        anns = item.annotations.upload(builder)

        # add label if still not in the dataset
        try:
            if item_label not in [l.tag for l in dataset.labels]:
                dataset.add_label(item_label)
        except Exception:
            print("failded to add {l} as label".format(l=item_label))



def cifar100_extract():
    with open(os.path.join(source_dir_path, 'meta'), 'rb') as f:
        meta = pickle.load(f, encoding='bytes')

    labels = {'coarse': [l.decode('utf-8') for l in meta[b'coarse_label_names']],
              'fine': [l.decode('utf-8') for l in meta[b'fine_label_names']],
             }
    base_dir = os.path.basename(source_dir_path)
    # Train-data
    if not args.skip == 'train':
        print("extrancting train")
        batch_path = os.path.join(source_dir_path, "train")
        cifar_100_extract_batch(batch_path=batch_path, labels=labels, remote_base=base_dir+'-train')


    # Test-data
    if not args.skip == 'val':
        print("extrancting test")
        test_batch_path = os.path.join(source_dir_path, "test")
        cifar_100_extract_batch(batch_path=test_batch_path, labels=labels, remote_base=base_dir+'-test')


def mnist_ds_process(ds, name='train', label_map=None):
    tmp_local_path = '/tmp/sgl_mnist.png'
    X, y = ds
    for id, (img, lbl) in tqdm(enumerate(zip(X, y))):
        cv2.imwrite(tmp_local_path, img)
        remote_path = f"mnist-{name}/{id:05}.png"  # pad the index to 5 digits
        item = dataset.items.upload(local_path=tmp_local_path, remote_name=remote_path)

        # add classification annotation
        if label_map is None:
            label_name = str(lbl)
        else:
            label_name = label_map.get(lbl, "un-def")
        builder = item.annotations.builder()
        builder.add(annotation_definition=dl.Classification(label=label_name))
        anns = item.annotations.upload(builder)


def mnist_extract():
    """
    Upload the mnist dataset to
    """
    import tensorflow as tf
    (x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
    mnist_ds_process((x_train, y_train), name='train')


def fashion_mnist_extract():
    """
    Upload the fashion mnist dataset to
    """
    import tensorflow as tf
    label_map = {0: "T-shirt/Top", 1: "Trouser", 2: "Pullover", 3:  "Dress", 4: "Coat",
                 5: "Sandals", 6: "Shirt", 7: "Sneaker", 8: "Bag", 9: "Ankle Boots"}
    if len(dataset.labels) == 0:
        dataset.add_labels(list(label_map.values()))

    ds_train, ds_test = tf.keras.datasets.fashion_mnist.load_data()

    if not args.skip == 'train':
        mnist_ds_process(ds_train, name='train')
    if not args.skip == 'val':
        mnist_ds_process(ds_test, name='test')

def gloun_ds_single_img(img_dataset, img_id, ds_name='pascal', ds_type='train'):
    from matplotlib import pyplot as plt
    image, label = img_dataset[img_id]  # Note image is of mxnet.ndarray type...
    bounding_boxes = label[:, :4]
    # print('Bounding boxes (num_boxes, x_min, y_min, x_max, y_max):\n', bounding_boxes)
    class_ids = label[:, 4:5]
    label_names = [img_dataset.classes[int(cls_id)] for cls_id in class_ids]

    local_path = f'/tmp/single-{ds_name}.png'
    remote_base = f'/{ds_name}_{ds_type}'
    remote_fname = '{ds_t}_{img_id:05}.png'.format(ds_t=ds_type, img_id=img_id)

    plt.imsave(local_path, image.asnumpy())
    item = dataset.items.upload(local_path=local_path, remote_path=remote_base, remote_name=remote_fname, overwrite=True)

    # add classification annotation
    builder = item.annotations.builder()
    for ann_id in range(len(class_ids)):
        builder.add(annotation_definition=dl.Box(
            bounding_boxes[ann_id][0], bounding_boxes[ann_id][1], bounding_boxes[ann_id][2], bounding_boxes[ann_id][3],
            label=label_names[ann_id]))
    anns = item.annotations.upload(builder)

    return 1


def pascal_voc_extract():
    from gluoncv import data, utils

    print("searching the dataset in {}".format(source_dir_path))
    train_dataset = data.VOCDetection(splits=[(2007, 'trainval'), (2012, 'trainval')], root=source_dir_path)
    test_dataset = data.VOCDetection(splits=[(2007, 'test')], root=source_dir_path)
    #print('Num of training images:', len(train_dataset))
    # print('Num of validation images:', len(val_dataset))

    try:
        dataset.add_labels(train_dataset.classes)
    except dl.exceptions.InternalServerError:
        print("did not add labels to dataset {}".format(dataset.name))

    nof_train, nof_test = len(train_dataset), len(test_dataset)

    # dataset._client_api.event_loops('items.upload')  # 13-aug Or suggestion debug
    if not args.skip == 'train':
        with Pool(processes=32) as pool:
            jobs = list()
            for train_id in tqdm(range(nof_train)):
                #gloun_ds_single_img(train_dataset, train_id, ds_type='train')
                p = pool.apply_async(gloun_ds_single_img, args=(train_dataset, train_id), kwds={'ds_type': 'train'})
                jobs.append(p)


    if not args.skip == 'val':
        for test_id in tqdm(range(nof_test)):
            gloun_ds_single_img(test_dataset, test_id, ds_type='test')


def ms_coco_extract():
    from gluoncv import data, utils

    # coco is in default:   ~/.mxnet/datasets/
    train_dataset = data.COCODetection(splits=['instances_train2017'])
    val_dataset = data.COCODetection(splits=['instances_val2017'])
    if not dataset.labels:
        try:
            dataset.add_labels(train_dataset.classes)
        except dl.exceptions.InternalServerError:
            print("did not add labels to dataset {}".format(dataset.name))

    nof_train, nof_val = len(train_dataset), len(val_dataset)
    if not args.skip == 'train':
        # pool = Pool(processes=10)
        # print(f"created pool w/ {pool._processes} processes")
        # jobs = list()
        for train_id in tqdm(range(nof_train)):
            gloun_ds_single_img(train_dataset, train_id, ds_type='train')
            # p = pool.apply_async(gloun_ds_single_img, args=(train_dataset, train_id), kwds={'ds_type': 'train'})
            # jobs.append(p)
        # pool.close()
        # pool.join()
        #while np.sum([job.get(timeout=1) for job in jobs]) < nof_train:
        #    time.sleep(1)

    if not args.skip == 'val':
        pool = Pool(processes=10)
        print(f"created pool w/ {pool._processes} processes for validataions")
        jobs = list()
        for val_id in tqdm(range(nof_val)):
            p = pool.apply(gloun_ds_single_img, args=(val_dataset, val_id), kwds={'ds_type': 'val'})
            #gloun_ds_single_img(val_dataset, val_id, ds_type='val')
            jobs.append(p)
        pool.close()
        # while np.sum([job.get(timeout=1) for job in jobs]) < nof_val:
        #     time.sleep(1)
        pool.join()


def flickr_extract():
    import pandas as pd
    df = pd.read_csv(f"{source_dir_path}/results.csv")

    if not args.skip == 'train':
        dataset.items.upload(local_path=f"{source_dir_path}/flickr30k_images")

    if not args.skip == 'annotations':
        pages = dataset.items.list()
        for page in pages:
            for item in page:
                tmp = df[df['image_name'] == item.name]
                builder = item.annotations.builder()
                for i, row in tmp.iterrows():
                    builder.add(annotation_definition=dl.Description(text=row[' comment']))
                anns = item.annotations.upload(builder)



if __name__ == '__main__':
    import argparse
    import functools

    # noinspection PyTypeChecker
    parser = argparse.ArgumentParser(
            description='automatic creation of contest datasets',
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('--name', default='cifar-10', help='choose the name of dataset to load',
                        choices=['cifar-10', 'cifar-100', 'mnist', 'fashion-mnist', 'pascal-voc',
                                 'ms-coco', 'ms-coco2'],
                        )
    parser.add_argument('--source-dir', action='store', help='override the source dir (default reffer to \'shefi-mini-vm\'')
    parser.add_argument('--skip', action='store', default=None, choices=['train', 'val'],  help='skip one of the partitions')
    parser.add_argument('--env', '-e', action='store', default='prod', choices=['dev', 'rc', 'prod'], help='set dlp environment')
    args = parser.parse_args()

    print(args)

    if args.source_dir is None:
        source_dir_path = SOURCES.get(args.name)
    else:
        source_dir_path = args.source_dir

    # Set dlp objects
    # ===============
    print(f"working in {args.env!r}: project {DLP_CFG[args.env]['project']['name']}. Loading {args.name}")
    dl.setenv(args.env)
    project = dl.projects.get(DLP_CFG[args.env]['project']['name'])
    if args.env == 'dev':
        # in dev it's one dataset for all the contests
        dataset = project.datasets.get(DLP_CFG[args.env]['dataset']['name'])
    else:
        dataset = project.datasets.get(args.name)


    # uploaing the data
    print(f"uploading content from {source_dir_path} to {dataset.name}")
    {
        'cifar-10': cifar10_extract,
        'cifar-100': cifar100_extract,
        'mnist': mnist_extract,
        'fashion-mnist': fashion_mnist_extract,
        'pascal-voc': pascal_voc_extract,
        'ms-coco': ms_coco_extract,
        'ms-coco2': ms_coco_extract,    # due to weird crash..
    }.get(args.name, lambda: print("not a valid dataset"))()