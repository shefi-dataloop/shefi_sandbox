import dtlpy as dl
import pandas as pd
import urllib
import requests
from time import time
import json
from local_sandbox import my_analytics_utils as utils


def fashion_mnist_df_cols(df=None):
    """
    If df is passed - returns the df with my cols
    other wise returns the suggesgeted cols to show
    :param df:
    :return:
    """
    my_cols = ['time', 'label', 'duration', 'routeName', 'userId', 'role', 'action',]
    if df is None:
        return my_cols
    else:
        return df[my_cols]


def init_empty_query():
    query = {
        "match": {
            "action": "annotation.*",
            "itemId": None
        },
        "aggs": {
            "nested": [
                {"field": 'action', "type": 'terms'},
                {"field": 'duration', "type": 'stats'},
            ]
        }
    }
    print(json.dumps(query, indent=2))
    return query


def show_match_in_query(query):
    print(json.dumps({k:v for k,v in query["match"].items() if k != "itemId"}, indent=2))

def format_query(query):
    msg=''
    for k in query.keys():
        if k == "match":
            msg += "match:\n" + json.dumps({k: v for k, v in query["match"].items() if k != "itemId"}, indent=2)
        else:
            msg += "\n" + str(k) + ":\n" + json.dumps(query[k], indent=2)
    return msg


def create_query(match_keys=None, max_size=1000, **kwargs):
    """Create manually query payload"""

    match = {"action": "annotation.*"}
    if match_keys is not None:
        for k, v in match_keys.items():
            match[k] = v
    aggs = {"nested": [{"field": k, "type": v} for k, v in kwargs.items()]}

    query = {
        "size": max_size,
        # "startTime": int(start_time * 1000),
        # "endTime": int(end_time * 1000),
        # add sub-dicts
        'match': match,
             }

    # TODO: action should be before label
    if len(aggs["nested"]):
        query['aggs'] = aggs

    # "startTime": 'now - 1w',
    # "endTime": current,
    return query


def create_samp_query(dataset: dl.entities.dataset = None, project: dl.entities.project = None):
    """Two versions of sample queries matching the structure i got from Or"""

    if dataset is not None:
        filters = dl.Filters(field='metadata.system.annotationStatus', values='completed')
        pages = dataset.items.list(filters=filters)

        # query_type = "samples"   # aggs - not working
        match_keys = {
            "datasetId": dataset.id,
            "itemId": [item.id for item in pages.all()],
        }
        query = create_query(match_keys=match_keys, action='terms', label="terms", duration='stats')

    elif project is not None:
        task = project.tasks.get('poc-clear-task2')
        query = create_query(match_keys={"taskId": task.id}, max_size=10000)

    else:
        query = create_query()

    return query


def get_fashion_poc_analytics(dataset: dl.entities.dataset, query=None, query_type='aggs'):
    project = dataset.project

    if query is None:
        query = create_samp_query(dataset=dataset)
        print(format_query(query))

    df = project.analytics.get_samples(query=query, return_field=query_type)

    return df


def call_analytics(project: dl.entities.project, query=None, return_df=None):
    """
    direct call to analytics end point returns responce
    :param project: project to query
    :param query: query `dict` to call analytics- default None uses specific task
    :param return_df: `str` that parses the return json into pandas.DataFrame
                      default is None, options = 'samples', 'aggs'
    """

    if query is None:
        query = create_samp_query(project=project)

    response = requests.post(f"{dl.environment()}/projects/{project.id}/analytics/itemQuery",
                             headers=dl.client_api.auth,
                             json=query)


    if response.status_code == 200:
        return response.json()
    else:
        raise RuntimeError(f"failed to get analytics response. code {response.status_code}")


def parse_response_to_df(resp_dict):
    # query["match"]["itemId"] = df.ann_item_id.to_list()
    # all_items_lytics = lytics.call_analytics(project, query=query)

    aggs = resp_dict["aggs"]
    lvl1 = list(aggs.keys())[0]   # TODO - i think this shold always be w/ len 1
    term_list_lvl1 = list(aggs[lvl1].keys())
    # create df if only one level
    if "grades_stats" in aggs[lvl1][term_list_lvl1[0]]:
        lytic_df = pd.DataFrame([aggs[lvl1][k]["grades_stats"] for k in term_list_lvl1], index=list(term_list_lvl1))
    else:  # support upto 2 levels of hierarchy
        raise RuntimeError("Does not support yet for 2 terms")
        lvl2 = list(aggs[lvl1][term_list_lvl1[0]].keys())[0]
        term_list_lvl2 = []
        #term_list_lvl1 = list(aggs[lvl1].keys())
        for k in term_list_lvl1:
            term_list_lvl2 += list(aggs[lvl1][k][lvl2].keys())
        term_list_lvl2 = list(set(term_list_lvl2))

        if "grades_stats" in aggs[lvl1][term_list_lvl1[0]][lvl2][term_list_lvl2[0]]:
            lytic_df = pd.DataFrame([aggs[lvl1][k1][lvl2][k2]["grades_stats"] for k1 in term_list_lvl1 for k2 in aggs[lvl1][k1][lvl2].keys()], index=list(term_list_lvl1+term_list_lvl2))





    # Convert all duration times from ms -> s
    lytic_df = lytic_df.apply(lambda x: round(x/1000, 2))
    lytic_df['count'] = lytic_df['count'].apply(lambda x: int(x*1000))

    hierarchical_ind = pd.MultiIndex.from_tuples([t.split('.') for t in term_list_lvl1])
    lytic_df.index = hierarchical_ind

    return lytic_df


def get_all_hier(aggs):
    from collections import namedtuple
    Level = namedtuple("level", ["field", "values"])
    levels = []
    dd = aggs
    while 'grade_stats' not in dd:
        fld = list(dd.keys())[0]
        #    lvls.append({fld: list(dd[fld].keys())})
        lvl = Level(fld,  list(dd[fld].keys()))
        levels.append(lvl)
        dd = dd[lvl.field][lvl.values[0]]
        break

    return levels

def main():
    """ Query and investigate the fashion-mnist poc"""
    project = dl.projects.get('shefi-contests')
    clear_ds = project.datasets.get('fashion-mnist-poc-clear')
    sugg_ds = project.datasets.get('fashion-mnist-poc-suggestion')
    ref_ds = project.datasets.get('fashion-mnist')

    sugg_task = project.tasks.get('poc-suggestion-task1')
    clear_task = project.tasks.get('poc-clear-task2')


    clear_query = create_samp_query(dataset=clear_ds)
    sugg_query = create_samp_query(dataset=sugg_ds)

    clear_aggs_df = get_fashion_poc_analytics(dataset=clear_ds, query=clear_query, query_type='aggs')
    sugg_aggs_df = get_fashion_poc_analytics(dataset=sugg_ds, query=sugg_query, query_type='aggs')

    clear_aggs_dict = clear_aggs_df.label[0]
    sugg_aggs_dict = sugg_aggs_df.label[0]

    print("showing annotation stats")
    print("stats for the clear data set")
    _ = utils.emprical_aggs_parse(clear_aggs_dict)
    print("stats for the suggestion data set")
    _ = utils.emprical_aggs_parse(sugg_aggs_dict)

    # Change to all item actions:
    clear_query["match"]["action"] = "item.*"
    sugg_query["match"]["action"] = "item.*"

    clear_aggs_df = get_fashion_poc_analytics(dataset=clear_ds, query=clear_query, query_type='aggs')
    sugg_aggs_df = get_fashion_poc_analytics(dataset=sugg_ds, query=sugg_query, query_type='aggs')

    clear_aggs_items_dict = clear_aggs_df.label[0]
    sugg_aggs_items_dict = sugg_aggs_df.label[0]


    print(f"Clear Task:\n\t",
          f"Annotation total duration: {utils.convert_sec(clear_aggs_items_dict['completed']['action']['item.completed']['grades_stats']['sum'])}",
          f"(amnt: {clear_aggs_items_dict['completed']['action']['item.completed']['grades_stats']['count']} ",
          f" Avg:  {clear_aggs_items_dict['completed']['action']['item.completed']['grades_stats']['avg']/1000:1.0f}s per item )"
          )
    print(f"Suggestion Task:\n\t",
          f"Annotation total duration: {utils.convert_sec(sugg_aggs_items_dict['completed']['action']['item.completed']['grades_stats']['sum'])}",
          f"(amnt: {sugg_aggs_items_dict['completed']['action']['item.completed']['grades_stats']['count']} ",
          f" Avg:  {sugg_aggs_items_dict['completed']['action']['item.completed']['grades_stats']['avg']/1000:1.0f}s per item )"
          )

    return clear_aggs_items_dict, sugg_aggs_items_dict


"""
TRUNK

    query={"match": {"action": "annotation*",
                     "datasetId": dataset.id,
                     "itemId": [item.id for item in pages.all()]
                     },
           "aggs": {"nested": [
               {"field": "label", "type": "terms"},
               {"field": "action", "type": "terms"},
               # {"field": "userId", "type": "terms"},
               # {"field": "time", "type": "histogram"},
               {"field": "duration", "type": "stats"}
           ]
           }
           }
)



payload = {
    "match": {
        "taskId": task_id,
        "assignmentId": assignment.id,
        "action": "item.completed"},
    "startTime": int(start_time * 1000),
    "endTime": int(end_time * 1000),
    "size": 1000
"""