import faiss
import os
import dtlpy as dl
import pickle
import json
import random
import deprecation
from tqdm import tqdm
from time import time
import cv2
from PIL import Image
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
import keras
from keras import Model
from sklearn import metrics

import tensorflow as tf
tf.get_logger().setLevel('WARNING')

from keras.applications import resnet50
from shefi_sandbox.shefi_logging import get_script_logger
logger = get_script_logger('QC-faiss', to_file=True)

IN_SHAPE = (224, 224, 3)  # defulat for imagenet
BATCH_SIZE = 64
IMAGE_BATCH = 500
features_df_path = '/tmp/features.pickle'


def load_data(source='fashion_mnist'):

    if source == 'fashion_mnist':
        from keras.datasets import fashion_mnist
        train_ds, test_ds = fashion_mnist.load_data()
        return train_ds, test_ds

    elif source == 'Bolet':
        dl.setenv('prod')
        project = dl.projects.get('Bolet')
        train_ds = project.datasets.get('300_train_25_07_19')   # 'original'
        test_ds =  project.datasets.get('pilot7')
        return train_ds, test_ds
    elif source == 'Foyer':
        dl.setenv('dev')
        project = dl.projects.get('Shefi_onboard')
        train_ds = project.datasets.get('copy of Foyer - Foyer')
        test_ds = project.datasets.get('copy of Foyer - Test Images')
        return train_ds, test_ds
    else:
        raise RuntimeError(f"dataset {source} is not implemented")


def get_source_filter(ds_source, phase='train'):
    filter = None
    if ds_source == 'Foyer':
        if phase == 'train':
            filter = dl.Filters(field='filename', values='/Dataset_1*')

    return filter


def get_label_map_local(ds_source='fashion_mnist'):
    if ds_source == 'fashion_mnist':
        label_map = {0: "T-shirt/Top", 1: "Trouser", 2: "Pullover", 3:  "Dress", 4: "Coat",
                     5: "Sandals", 6: "Shirt", 7: "Sneaker", 8: "Bag", 9: "Ankle Boots"}
        inv_map = {v: k for k, v in label_map.items()}
        return label_map, inv_map
    else:
        raise RuntimeError(f"{ds_source} does not have a known label map")


def get_label_map(dataset:dl.entities.dataset):
    """ Returns label mapping from name to int (and inv) based on the ontology
        special mode for `Bolet` data using dir structure"""
    import os

    name2id_map = {}
    if dataset.labels:
        # we can parse the onthology..
        for id, label in enumerate(dataset.labels):
            name2id_map[label.tag] = id
    else:
        # Build the label mapping - Used in Boyer Dataset
        filter = dl.Filters(field='type', values='dir')
        pages_dir_names = dataset.items.list(filters=filter)
        id = 0
        #for item_id, item in enumerate(pages_dir_names.all())  # TODO: Ask Or is there a way to enumarte the actual items?
        for page in pages_dir_names:
            for item in page:
                label_name = os.path.basename(item.filename)
                if label_name not in name2id_map:
                    name2id_map[label_name] = id  # AKA inv_label_map
                    id += 1

    label_map = {v: k for k, v in name2id_map.items()}
    return label_map, name2id_map


def get_model(model_name='resnet'):
    from keras.applications.resnet50 import ResNet50
    """Returns the last fully connected layer (aka features vec) of a known model"""
    if model_name == 'resnet':
        model = ResNet50(include_top=True,    # False is useful for fine-tune training w/ different classes
                         input_shape=IN_SHAPE,
                         weights='imagenet',
                         )
    else:
        raise RuntimeError(f"model {model_name!r} not supported yet")

    feature_model = Model(inputs=[model.layers[0].input],
                          outputs=[model.layers[-2].output],
                          name=f"{model_name}_features_{model.layers[-2].output_shape[-1]}_model")
    return feature_model


def prepare_images(images, model_name='resnet'):
    """
    Basic preprocess wrapper.
    converts images to proper channel requirments and model preprocess function
    :param images: numpy.ndarray or list of string of images (local mode)
    """
    from tensorflow.keras.preprocessing import image

    # TODO: add labels from the dataset
    assert (images is not None or len(images) == 0), "images can not be empty"

    if isinstance(images[0], str):
        x = []
        for img_path in images:
            img = image.load_img(img_path, target_size=IN_SHAPE[:2])  # read image and reshape (default is 'nearest')
            np_img = image.img_to_array(img)  # convert PIL object to numpy
            x.append(np_img)
    elif isinstance(images[0], np.ndarray):
        images_arr = []
        try:
            for i,img in enumerate(images):
                if len(img.shape) == 2:  # Gray scale image:
                    img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
                elif img.shape[2] == 4:
                    img = cv2.cvtColor(img, cv2.COLOR_RGBA2BGR)
                images_arr.append(cv2.resize(img, IN_SHAPE[:2]))
        except Exception as e:
            logger.critical(f"Failed to prepare image {i} in batch... ")
            raise e

        x = np.asarray(images_arr)
    else:
        raise RuntimeError(f"images type {type(images[0])} is not supported. either sting for filepath or np.ndarry")

    if not x.ndim > len(IN_SHAPE):
        # add 1st dim for batch (usualy due to 1 img in list)
        x = np.expand_dims(x, axis=0)  # add 0 axis - batch like  => shape  =  (1, row, cols chnl)

    if model_name == 'resnet':
        images_model = resnet50.preprocess_input(x)
    else:
        raise RuntimeError(f"model {model_name} not supported")

    images_dataset = images_model

    return images_dataset


def prepare_features_df_local(model, images, labels, label_map=None, features_df=None):

    columns = ['label_id', 'label_name', 'features']
    if label_map is None:
        label_map = {}

    df = pd.DataFrame(columns=columns)
    for i in tqdm(range(0, len(labels), BATCH_SIZE), desc='batching features', total=int(len(labels)/BATCH_SIZE), ncols=90):
        batch_labels = labels[i:i+BATCH_SIZE]
        batch_label_names = [label_map.get(lbl) for lbl in batch_labels]
        batch_images = prepare_images(images[i:i+BATCH_SIZE])  # TODO: add the model name
        batch_features = model.predict([batch_images])

        series_to_append = [pd.Series([batch_labels[i], batch_label_names[i], batch_features[i]], index=columns) for i in range(len(batch_features))]
        df = df.append(series_to_append, ignore_index=True)

    logger.info(f"Creating features df from local ds ended. df shape = {df.shape}")

    if features_df is None:
        return df
    else:
        return pd.concat([features_df, df])


def proc_single_dlp_item(item, inv_label_map, lock, batch_dict, label_converter=None):
    # OBSOLETE CODE - Used in 'Bolet' ds where label we're used by dir structure
    # label_name = os.path.basename(item.dir)
    # label_id = inv_label_map.get(label_name)

    annotations = item.annotations.list()
    label_name = [ann.label for ann in annotations if ann.label not in ['completed', 'approved']]
    if label_converter is not None:
        label_id = [int(inv_label_map.get(label_converter(lbl_name), -1)) for lbl_name in label_name]  # ToDo should i save it a numpy array?
    else:
        label_id = [int(inv_label_map.get(lbl_name, -1)) for lbl_name in label_name]                   # ToDo should i save it a numpy array?

    # download item as a buffer and save as np array
    buffer = item.download(save_locally=False)
    img_np = np.asarray(Image.open(buffer))

    with lock:
        batch_dict['images'].append(img_np)
        batch_dict['label_ids'].append(label_id)
        batch_dict['label_names'].append(label_name)
        batch_dict['items_ids'].append(item.id)


def prepare_features_df_dlp(model, dataset, inv_label_map, features_df=None, filter=None, label_converter=None):
    """

    :param dataset: dl.entities.dataset.Dataset object
    :param inv_label_map:  dictionary mapping from label name to id
    :param features_df:  pandas.DataFrame object - used to concatente existing feature df
    :param filter: specific dl.Filter object to be used on dataset (if needed)
    :return: pd.DataFrame with all features extracted from images
    """
    import os
    import cv2
    import threading
    from multiprocessing.pool import ThreadPool as Pool

    dl.verbose.disable_progress_bar = True
    dl.verbose.logging_level = 'error'

    columns = ['itemId', 'label_id', 'label_name', 'features']
    df = pd.DataFrame(columns=columns)
    if filter is None:
        filter = dl.Filters()
    # make sure only image items are passed
    filter.add(field='metadata.system.mimetype', values='image*')

    tic = time()
    batches = dataset.items.list(filters=filter, page_size=BATCH_SIZE)
    logger.info(f"creating feature vectors for {batches.items_count} items, using {model.name}\n")
    for batch in tqdm(batches, desc='batches', total=np.ceil(batches.items_count/BATCH_SIZE), ncols=90):
        batch_dict = {'items_ids': [], 'label_ids': [], 'label_names': [], 'images': []}
        items_debug =[]
        lock = threading.Lock()
        p = Pool(processes=16)
        for item in batch:
            items_debug.append(item)
            p.apply_async(proc_single_dlp_item,
                          args=(item, inv_label_map, lock, batch_dict),
                          kwds={'label_converter': label_converter})
        p.close()
        p.join()
        # Process the Batch
        try:
            batch_images = prepare_images(batch_dict['images'])  # TODO: add the model name
        except Exception as e:
            logger.critical("failed to prepare images from batch to the frame work")
            batch.print()
            url_links = [dl.client_api._get_resource_url('item', project_id=item.dataset.project.id,  dataset_id=item.dataset.id, item_id=item.id) for item in items_debug]
            logger.error(f"Items ids = {batch_dict['items_ids']}\n{url_links}")
            raise e
        batch_features = model.predict([batch_images])

        series_to_append = [
            pd.Series([
                batch_dict['items_ids'][i],
                batch_dict['label_ids'][i],
                batch_dict['label_names'][i],
                batch_features[i]
            ], index=columns) for i in range(len(batch_features))]
        df = df.append(series_to_append, ignore_index=True)
    toc = time()

    logger.info(f"Creating features df from dlp ended. df shape = {df.shape} ({toc-tic:.2f}s)")

    if features_df is None:
        return df
    else:
        return pd.concat([features_df, df], ignore_index=True)


def load_index(feature_df, index=None, model=None):
    import faiss                   # make faiss available

    if index is None:
        if model is None: raise NotImplementedError("can't build an index w/o model, please pass either as not None")
        features_len = model.layers[-1].output_shape[1]
        index = faiss.IndexFlatL2(features_len)   # build the index
        logger.info(f"Preparing index. feature size = {features_len}")

    features = np.array(feature_df.features.to_list())
    index.add(features)

    logger.info(f"index summary:\nINDEX size {index.ntotal} samples ({feature_df.shape[0]} were added)\n\tis trained: {index.is_trained}")
    return index


def query_index(index, queries_df, index_df, multi_label=False, return_search=False):
    """
    Preforms the quering on the index, returns the retured labels (or list of labels if multi-label)
    :param index:  Faiss index
    :param queries_df: df used for the index
    :param index_df:  df to be used to query
    :param multi_label: Bool if we are using mulit-labels or not
    :param return_search:  if True, returns alson the I and D matrices from the seaech
    :return: np.ndarray with the result labels returned form the nn search in the index
    """

    if multi_label:
        knows_labels = set()
        for l in index_df.label_id.to_list():
            knows_labels = knows_labels.union(l)
    else:
        knows_labels = index_df.label_id.unique().astype(np.int)   # TODO: fix in multilabel
        knows_labels = knows_labels.sort()


    non_traind_df = queries_df[queries_df['label_id'].isnull()]  # there are samples that has no annotations in the train set
    queries_df.dropna(subset=['label_id', 'features'], inplace=True)  # Remove all the non trained items

    queries = np.array(queries_df.features.to_list())
    D,I = index.search(queries, k=5)

    return_labels_type = np.int if multi_label is False else np.ndarray

    query_returned_labels = np.zeros(I.shape, dtype=return_labels_type)
    for i in range(I.shape[0]):
        for j in range(I.shape[1]):
            query_returned_labels[i, j] = index_df.label_id.iloc[I[i, j]]

    if return_search:
        return query_returned_labels, I, D
    else:
        return query_returned_labels


def calc_accuracies(queries_df: pd.DataFrame, query_returned_labels: np.ndarray, multi_label: bool = False):
    return_labels_type = query_returned_labels.dtype
    nof_queries, knn = query_returned_labels.shape

    act = queries_df.label_id.to_numpy().astype(return_labels_type)
    prd_1nn = query_returned_labels[:, 0]

    # TODO use statistics.mode
    prd_maj_vote = np.zeros(nof_queries, dtype=return_labels_type)  # All nearest neigbours returned from index  vote for majority consensus
    for samp in range(nof_queries):
        knn_vote = label_voting(query_returned_labels[samp])
        if multi_label:
            prd_maj_vote[samp] = [0] if not knn_vote else knn_vote
        else:
            prd_maj_vote[samp] = 0 if not knn_vote else knn_vote[0]

    # top x accuracy (either one of the top results is correct)
    # =========================================================
    top_3_accuracy = top_x_prd(act, query_returned_labels, x=3)
    # top_5_accuracy = top_x_prd(act, query_returned_labels, x=5)
    # =================================================

    if multi_label:
        accuracy = top_x_prd(act, query_returned_labels, x =1)
        maj_vote_accuracy = np.count_nonzero(prd_maj_vote == act) / len(act)
        nn_dist_acc = multi_label_dist_accuracy(act, prd_1nn, normalize='total')
    else:
        accuracy = metrics.accuracy_score(act, prd_1nn)
        maj_vote_accuracy = metrics.accuracy_score(act, prd_maj_vote)
        nn_dist_acc = accuracy


    # Find Errors
    # ===========

    logger.info(f""" results summary:
        first nn accuracy  : {accuracy*100:2.1f}%
        knn voting accuracy: {maj_vote_accuracy*100:2.1f}%
        top 3 accuracy     : {top_3_accuracy*100:2.1f}%   (either one of top 3 is correct)
        nn dist accuracy   : {nn_dist_acc*100:2.1f}%   (how many correct labels in the intersection / prd U act)
    """)
    # actual labels     = {queries_df.label_id.to_numpy()[rand_ind]}
    # first nn in index = {query_returned_labels[:,0][rand_ind]}


def create_error_df(queries_df: pd.DataFrame, query_returned_labels: np.ndarray, error_indices=None):
    """

    :param queries_df: `pd.DataFrame` of all the items and their
    :param query_returned_labels:
    :param error_indices:
    :return:
    """
    return_labels_type = type(query_returned_labels[0])
    act = queries_df.label_id.to_numpy() # .astype(return_labels_type)
    # prd_1nn = query_returned_labels[:, 0]

    if error_indices is None:
        # default method for error indecies - top-3 error
        error_indices = top_x_prd(act, query_returned_labels, x=3, return_list=True) == 0

    error_df = queries_df[error_indices].copy()
    error_df['predictions'] = query_returned_labels[error_indices, 0]
    if issubclass(return_labels_type, np.ndarray):  # is_multi_label
        sym_diff_f = lambda row: set(row.label_id).symmetric_difference(set(row.predictions))
        act_diff_f = lambda row: set(row.label_id).difference(set(row.predictions))
        prd_diff_f = lambda row: set(row.predictions).difference(set(row.label_id))
        error_df['sym_diff'] = error_df.apply(sym_diff_f, axis=1)
        error_df['act_diff'] = error_df.apply(act_diff_f, axis=1)  # act - prd: in actual but not in prd
        error_df['prd_diff'] = error_df.apply(prd_diff_f, axis=1)

    logger.info(f"created error DataFrame with shape {error_df.shape}")
    return error_df

def sample_view(queries_df, query_returned_labels, nof_test_case=10, label_map=None):
    multi_label = (query_returned_labels.dtype == np.ndarray)
    # Create a sample to view
    # =======================
    #pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 100)
    pd.set_option('display.width', 200)
    rand_ind = np.random.permutation(queries_df.shape[0])[:nof_test_case]
    sample_df = queries_df.iloc[rand_ind].copy()
    sample_df['predictions'] = query_returned_labels[rand_ind, 0]
    if label_map is not None:
        if multi_label:
            func = lambda lbls: [label_map[lbl] for lbl in lbls]
        else:
            func = lambda lbl: label_map[lbl]
        sample_df['prediction_names'] = sample_df.predictions.apply(func)
    cols_to_show = [col for col in ['itemId', 'label_id', 'label_name', 'predictions', 'prediction_names'] if col in sample_df.columns]

    logger.debug(f"showing the results for sample of {nof_test_case} cases:\n{sample_df[cols_to_show].head(nof_test_case)}")
    return sample_df

def label_voting(predictions):
    """ Predictions is list / list of list (in case of multi label per image)
        The length is the amount of returned nn from index - the voting should return if more than half voted for that label"""

    d = {}
    for lbl in predictions:
        if isinstance(lbl, list) or isinstance(lbl, np.ndarray):  # list - multi label
            for m_lbl in lbl:
                if m_lbl in d:
                    d[m_lbl] += 1
                else:
                    d[m_lbl] = 1
        else:
            if lbl in d:
                d[lbl] += 1
            else:
                d[lbl] = 1
    # Voting
    res = []
    for k,v in d.items():
       if v > len(predictions)/2:
           res.append(k)
    return res


def top_x_prd(act, prd_labels, x: int = 3, return_list=False) -> float:
    """
    calculates the top-x predictions, whether this is a correct predicition at one of top search results

    :param act: `list` of actual label (or `list` of lists` in case of multi-labels)
    :param prd_labels: `list` of predicted labels (or `list` of lists` in case of multi-labels)
    :param x: `int` how many search result to check
    :param return_list:  `If true, returns the actual `pd.Series` for each of the `act` list
    :return: `float` representing the accuracy or `pd.Series`, in case return_list is `True`
    """
    nof_queries = act.shape[0]
    top_x_prd = np.zeros(nof_queries, dtype=np.int)
    for r in range(x):
        top_x_prd += prd_labels[:, r] == act
    top_x_accuracy = np.count_nonzero(top_x_prd) / nof_queries
    if return_list:
        return top_x_prd
    return top_x_accuracy


def multi_label_dist_accuracy(act_labels, prd_labels, method='len', normalize='act', return_list=False) -> float:
    """
    Calculates the accuracy as a continues float [0-1].
     the float represent the similarity between act and prd list: 1- they are the same; 0- totally disjoint

    :param act_labels: `list` of 'lists` inner `list` in the acutal multi-labels ids; outer are the different samples
    :param prd_labels:  `list` of 'lists` inner `list` in the predicted multi-labels ids; outer are the different samples
    :param normalize: `str`: total / act - how to normalize the intersection (only by the size of actuals or doubel sided)
    :param method: how to calculate the dist
        'len': dist is measures as the ratio between the error list to (act | prd)
        'area': add weight to each label in multi-label by it's area
    :param return_list:  if True, returns the entire list w/o averaging
    :return: accuracy by dist. 1 is no errors, 0 is totally wrong
    """

    accuracies = []
    for act, prd in zip(act_labels, prd_labels):
        act, prd = set(act), set(prd)
        correct_prd = act.intersection(prd)
        acc_of_act = len(correct_prd) / len(act)
        acc_of_tot = len(correct_prd) / len(act | prd)
        if normalize == 'total':
            accuracies.append(acc_of_tot)
        elif normalize == 'act':  # This version ignores the case where act is a subset of prd
            accuracies.append(acc_of_act)
        else:
            accuracies.append(acc_of_act)

    if return_list:
        return np.array(accuracies)

    return np.mean(accuracies)


@deprecation.deprecated(details='please use `create_error_report` html formatting is part of Jinja')
def error_df2html_not_fake(error_df: pd.DataFrame, inv_map: dict = None, return_as_str=False):
    """This version ignorres the bug where item.dataset may be wrong and preforms slow query for all items"""
    outout_columns = ['item_link', 'label_id', 'label_name', 'predictions', 'act_diff', 'prd_diff']

    if inv_map is not None:
        label_map = {v: k for k, v in inv_map.items()}
        ids_to_names = lambda lbls: [label_map.get(lbl, 'UN-DEF') for lbl in lbls]
        error_df['predict_names'] = error_df.predictions.apply(ids_to_names)
        # update the output columns as we don't need the ids not more
        outout_columns.remove('predictions')
        outout_columns.remove('label_id')
        outout_columns += ['predict_names']
        if 'sym_diff' in error_df.columns:
            logger.debug("converting sym_diff from ids to names")
            error_df['sym_diff'] = error_df.sym_diff.apply(ids_to_names)
            error_df['prd_diff'] = error_df.prd_diff.apply(ids_to_names)
            error_df['act_diff'] = error_df.act_diff.apply(ids_to_names)

    debug_get_timer = []
    debug_timer = []
    import time
    def id_to_link_not_fake(item_id: str):
        item_str = 'item'
        tic = time.time()
        item = dl.items.get(item_id=item_id)
        toc = time.time()
        dataset = item.dataset
        # project = item.project
        project_id = dataset.projects[0]
        toc2 = time.time()
        link = f'<a href={dl.client_api._get_resource_url(item_str, project_id=project_id,  dataset_id=dataset.id, item_id=item_id)}>{item_id}</a>'
        debug_get_timer.append(toc-tic)
        debug_timer.append(toc2-toc)
        return link

    error_df['item_link'] = error_df['itemId'].apply(id_to_link_not_fake)

    # Prepare for output
    # ==================
    outout_columns = [c for c in outout_columns if c in error_df.columns]
    error_df.to_html(open('/tmp/error_df.html', 'w'), columns=outout_columns, escape=False)
    # DEBUGGER:
    debug_get_timer = np.array(debug_get_timer)
    debug_timer = np.array(debug_timer)
    logger.debug(f"called 'dl.item.get` {len(debug_get_timer)} times. avg time {np.mean(debug_get_timer):.2f}s (total: {np.sum(debug_get_timer):.2f}s) ")
    logger.debug(f"for {len(debug_timer)} items, get the dataset and project took avg time {np.mean(debug_timer):.2f}s (total: {np.sum(debug_timer):.2f}s) ")

    if return_as_str:
        return error_df.to_html(columns=outout_columns, escape=False)


@deprecation.deprecated(details='please use `create_error_report` html formatting is part of Jinja')
def output_error_df(error_df: pd.DataFrame, project: dl.entities.project, dataset: dl.entities.dataset, inv_map: dict = None):
    """Creates a local html file for debugging the dataset """

    outout_columns = ['item_link', 'label_id', 'label_name', 'predictions']
    if inv_map is not None:
        label_map = {v: k for k, v in inv_map.items()}
        error_df['predict_names'] = error_df.predictions.apply(lambda lbls: [label_map.get(lbl, 'UN-DEF') for lbl in lbls])
        # update the output columns as we don't need the ids not more
        outout_columns.remove('predictions')
        outout_columns.remove('label_id')
        outout_columns += ['predict_names']

    item_str = 'item'
    id_to_link = lambda item_id: \
        f'<a href={dl.client_api._get_resource_url(item_str, project_id=project.id,  dataset_id=dataset.id, item_id=item_id)}>{item_id}</a>'

    error_df['item_link'] = error_df['itemId'].apply(id_to_link)

    outout_columns = [c for c in outout_columns if c in error_df.columns]
    error_df.to_html(open('/tmp/error_df.html', 'w'), columns=outout_columns, escape=False)


def create_error_report(error_df: pd.DataFrame, inv_map: dict, dataset: dl.entities.dataset = None,
                        out_dir='/tmp', output_columns=None, desc='', source=''):
    """
    Creates a jinja2 template of the error df + legend
    defults for passing an inv_label map
    :param error_df: 
    :param dataset: 
    :param inv_map: 
    :param output_coloums: 
    :return: 
    """
    from jinja2 import Template
    from functools import partial

    if output_columns is None:
        output_columns = ['item_link', 'label_name', 'predict_names' ]

    def id_to_link(item_id: str, dataset: dl.entities.dataset):
        if dataset is None:
            # in case no dataset is given, retrive it (need 2 GET calls due to datasetId bug)
            item = dl.items.get(item_id=item_id)
            dataset = item.dataset
        project_id = dataset.projects[0]
        link = f"<a href={dl.client_api._get_resource_url('item', project_id=project_id, dataset_id=dataset.id, item_id=item_id)}>{item_id}</a>"
        return link

    # create a partial version with dataset passed as argument
    id_to_link = partial(id_to_link, dataset=dataset)
    error_df['item_link'] = error_df['itemId'].apply(id_to_link)

    # Convert labels ids to names
    label_map = {v: k for k, v in inv_map.items()}
    error_df['predict_names'] = error_df.predictions.apply(lambda lbls: [label_map.get(lbl, 'UN-DEF') for lbl in lbls])
    labels_df = pd.DataFrame(inv_map.values(), index=inv_map.keys(), columns=['label_id'])

    # make sure all columns are valid
    output_columns = [c for c in output_columns if c in error_df.columns]

    template = Template(open('qc-report.html', 'r').read())
    rendered = template.render(source_name=source,
                               desc=desc,
                               label_df=labels_df.transpose(),
                               error_df=error_df[output_columns])
    with open(f'{out_dir}/segmentation-err-from-template.html', 'w') as f:
        f.write(rendered)
    # TODO: complete


def foyer_indoor_outdoor_filt(error_df: pd.DataFrame):
    """Add another filtering tool for Indoor(61) and outdoor(51)"""
    exp_df = error_df.explode('label_id')
    exp_df = exp_df.explode('predictions')

    set_1 = exp_df[ (exp_df['label_id'] == 61) & (exp_df['predictions'] == 51) ]  # act indoor closest to outdoor
    set_2 = exp_df[ (exp_df['label_id'] == 51) & (exp_df['predictions'] == 61) ]

    ids = np.hstack((set_1.index, set_2.index))
    return error_df.loc[ids]


def foyer_compare_lists(error_df: pd.DataFrame):
    diff_ids = []
    for idx, row in error_df.iterrows():
        if not compare_lists(row['label_id'], row['predictions']):
            pass


def compare_lists(list_a, list_b, thr=2):
    """Return True if list are similar (by thr)"""
    diff_cnt = 0
    for l in list_a:
        if l not in list_b:
            diff_cnt += 1

    if diff_cnt >= thr:
        return False
    else:
        return True


def main(load_df=False, save_df=True, enable_query=False, max_train_size=None,
         ds_source='Bolet', model_info_dir='/tmp', model_name='resnet'):

    model = get_model(model_name=model_name)

    logger.info(f"loading data of {ds_source}")
    train_ds, test_ds = load_data(source=ds_source)
    features_df_path = os.path.join(model_info_dir,  f"features-{ds_source}-{model_name}.pickle")
    mapping_json_path = os.path.join(model_info_dir,  f"features-{ds_source}-{model_name}.json")
    label_map = None # init value

    if load_df:
        with open(features_df_path, 'rb') as f:
            df = pickle.load(f)
        logger.info(f"loaded index df form {features_df_path}. df shape = {df.shape}")
        try:
            inv_label_map = json.load(open(mapping_json_path, 'r'))
        except:
            logger.error(f"unable to load label_mapping form {mapping_json_path}")
    else:
        tic = time()
        if ds_source == 'fashion_mnist':
            label_map, inv_label_map = get_label_map_local(ds_source=ds_source)
            df = prepare_features_df_local(model=model, images=train_ds[0], labels=train_ds[1], label_map=label_map)
        elif ds_source in ['Bolet', 'Foyer']:
            label_map, inv_label_map = get_label_map(dataset=train_ds)
            filter = get_source_filter(ds_source)
            df = prepare_features_df_dlp(model=model, dataset=train_ds, inv_label_map=inv_label_map, filter=filter)
        else:
            raise RuntimeError(f"dataset {ds_source} is not implemented")

        toc = time()
        dur = toc - tic
        logger.info(f"prepare index df took  {(dur)//(60*60):n}:{(dur//60)%60:n}::{dur%60:1.2f} (h:m:ss)      ({toc - tic:.2f}s)")

    if max_train_size is not None:
        frac = max_train_size / df.shape[0]
        df = df.sample(frac=frac)

    if save_df:
        pickle.dump(df, open(features_df_path, 'wb'))
        json.dump(inv_label_map, open(mapping_json_path, 'w'))

    index = load_index(feature_df=df, index=None, model=model)

    if enable_query:
        tic = time()
        if ds_source == 'fashion_mnist':
            queries_df = prepare_features_df_local(model=model, images=test_ds[0], labels=test_ds[1], label_map=label_map)
        elif ds_source in ['Bolet', 'Foyer']:
            queries_df = prepare_features_df_dlp(model=model, dataset=test_ds, inv_label_map=inv_label_map, )
        else:
            raise RuntimeError(f"dataset {ds_source} is not implemented")
        if save_df:
            fname, ext = os.path.splitext(features_df_path)
            pickle.dump(queries_df, open(f"{fname}-test{ext}", 'wb'))
        toc = time()
        dur = toc - tic
        logger.debug(f"prepare queries df took  {(dur)//(60*60):n}:{(dur//60)%60:n}::{dur%60:1.2f} (h:m:ss)      ({toc - tic:.2f}s)")
        is_multi_label = (ds_source == 'Foyer')
        query_returned_labels = query_index(index=index, queries_df=queries_df, index_df=df, multi_label=is_multi_label)
        calc_accuracies(queries_df=queries_df, query_returned_labels=query_returned_labels, multi_label=is_multi_label)
        error_df = create_error_df(queries_df=queries_df, query_returned_labels=query_returned_labels)
        samp_df = sample_view(queries_df=queries_df, query_returned_labels=query_returned_labels, label_map=label_map)

    #output_error_df(error_df)
    # create_error_report(error_df=error_df, inv_map=inv_label_map, dataset=None,
    #                     desc=f"{ds_source} default report", source=ds_source)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description="faiss + classification feature vector poc",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument('--pkl_path', action='store', help='set path of the local index saved as pickle')
    parser.add_argument('--save', action='store_true', default=False, help="if passed saves the train df into pickle")
    parser.add_argument('--load', action='store_true', default=False, help="if passed load existing pickle (not computing) from pkl-path")
    parser.add_argument('--query', action='store_true', default=False, help="if passed executes the actual testing of faiss")
    parser.add_argument('--debug', action='store_true', default=False, help="set the logger to debug")

    parser.add_argument('--source', action='store', default='fashion_mnist', help='choose a source of the dataset, not all are avilable')
    parser.add_argument('--max-train', action='store', type=int, default=None, help='size of train images to use (in the index)')
    parser.add_argument('--output-dir', action='store', default='/tmp', help='output/input dir for the pickle files')

    args = parser.parse_args()

    if args.debug:
        from shefi_sandbox.shefi_logging import set_stream_handler
        set_stream_handler(logger, level='DEBUG')

    features_df_path = args.pkl_path if args.pkl_path is not None else features_df_path
    if not os.path.isdir(args.output_dir):
        os.makedirs(args.output_dir)

    main(save_df=args.save, load_df=args.load, enable_query=args.query, max_train_size=args.max_train,
         ds_source=args.source, model_info_dir=args.output_dir)



"""
TRUNK
=====
        # np.take(X_train,np.random.permutation(X_train.shape[0]),axis=0, out=X_train)  # inplace random on first dim
        #for i in range(0,train_ds[0].shape[0], IMAGE_BATCH):
        for i in range(0, 5000, IMAGE_BATCH):
            print(f"processing images {(i,i+IMAGE_BATCH)}")
            X_train = prepare_images(images=train_ds[0][i:i+IMAGE_BATCH])



  df.pivot_table(values=['Sales'], index=['Province', 'City'], columns=['City'], aggfunc=np.sum, margins=True ) 
"""