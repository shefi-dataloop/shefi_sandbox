"""
The script should support basic interfaces with the ipm insects model
"""
import dtlpy as dl
from tqdm import tqdm
import numpy as np
from PIL import Image
import sys,os
import deprecation

def load_model(model_path='/tmp/model.h5'):
    sys.path.append(os.path.expanduser('~/dlp_ws/keras-yolo3'))  # my local pc
    sys.path.append(os.path.expanduser('~/keras-yolo3'))         # vm: shefi-gpu-tf-1-15
    from yolo import YOLO
    import json

    dir_path = os.path.dirname(model_path)
    try:
        cfg = json.load(open(f'{dir_path}/config.json', 'r'))
    except FileNotFoundError:
        cfg = {}
    my_params = {
       "model_path":    model_path,
        "anchors_path": f'{dir_path}/yolo_anchors.txt',
        "classes_path": f'{dir_path}/classes.txt',
        "score": cfg.get('score', 0.5),
        # "iou": 0.45,
        "model_image_size": tuple(cfg.get('image_size', (416, 416))),
        # "gpu_num": 1,
        }

    yolo = YOLO(**my_params)

    # import keras
    # model = keras.models.load_model(model_path, compile=True)
    # return model
    return yolo


@deprecation.deprecated(details="We change the format to use with remote files...")
def annotate_dlp_local_image(dir_path, img_name, show=False):
    from matplotlib import pyplot as plt
    import json
    image = Image.open(os.path.join(dir_path,'items',img_name))
    ann_file = f"{dir_path}/json/{os.path.splitext(img_name)[0]}.json"
    with open(ann_file, 'r') as f:
        annotations = json.load(f)['annotations']

    annotations = [ann for ann in annotations if ann['type'] == 'box']
    boxes, classes = [], []
    for ann in annotations:
        # box = [top, left, bottom, right]
        box = [ann['coordinates'][0]["y"], ann['coordinates'][0]["x"], ann['coordinates'][1]["y"], ann['coordinates'][1]["x"]]
        boxes.append(box)
        classes.append(ann['label'])
    scores = ['GT']*len(boxes)

    if show:
        ann_image = draw_boxes_on_image(np.array(image), boxes, classes, scores)
        plt.imshow(ann_image)
        plt.show()
    return boxes, classes, scores, image


def get_gt_dlp_image(item: dl.entities.Item, show=False):
    """
    Process a remote item in dlp server and returns the boxes annotation and the image a PIL.Image
    :param item: `dl.Item`
    :param show: `bool` if passed show the image w/ pyplot
    :return: boxes, classes, scores, PIL.Image
    """
    image = Image.open(item.download(save_locally=False))
    annotations = [ann for ann in item.annotations.list() if ann.type == 'box']
    boxes, classes = [], []
    for ann in annotations:
        # box = [top, left, bottom, right]
        box = [ann.coordinates[0]["y"], ann.coordinates[0]["x"], ann.coordinates[1]["y"], ann.coordinates[1]["x"]]
        boxes.append(box)
        classes.append(ann.label)
    scores = ['GT']*len(boxes)

    if show:
        from matplotlib import pyplot as plt
        ann_image = draw_boxes_on_image(np.array(image), boxes, classes, scores)
        plt.imshow(ann_image)
        plt.show()
    return boxes, classes, scores, image


def predict(yolo, image: Image.Image, show=False):
    from keras import  backend as K
    from yolo3.utils import letterbox_image
    from matplotlib import pyplot as plt
    assert yolo.model_image_size[0]%32 == 0, 'Multiples of 32 required'
    assert yolo.model_image_size[1]%32 == 0, 'Multiples of 32 required'
    boxed_image = letterbox_image(image, tuple(reversed(yolo.model_image_size)))
    image_data = np.array(boxed_image, dtype='float32')

    image_data /= 255.
    image_data = np.expand_dims(image_data, 0)  # Add batch dimension.

    out_boxes, out_scores, out_classes = yolo.sess.run(
        [yolo.boxes, yolo.scores, yolo.classes],
        feed_dict={
            yolo.yolo_model.input: image_data,
            yolo.input_image_shape: [image.size[1], image.size[0]],
            K.learning_phase(): 0
        }
    )

    if show:
        ann_image = draw_boxes_on_image(np.array(image).copy(), boxes=out_boxes, classes=out_classes, scores=out_scores,
                                        all_class_names=yolo.class_names)
        plt.imshow(ann_image)
        plt.show()
    return out_boxes, out_classes, out_scores


def compare_prd(item: dl.entities.Item, my_yolo=None, out_dir='/tmp', show=False, iou_thr=0.8):
    """
    Compare a single image from dataloop dir structure.
    the compare alson creates stat dict for the image
    saves an image w/ both prd and ft annotations

    :param dir_path: root dir of the dataloop structure
    :param img_name: img file name as saved in dataloop
    :param my_yolo: YOLO object detector
    :param out_dir: local dir to save the annotated image
    :param show: boolean if to show the image
    :param iou_thr: thr whether to count a prediction as hit / miss
    :return: stat dict
    """
    from matplotlib import pyplot as plt
    gt_boxes, gt_classes, gt_scores, image = get_gt_dlp_image(item=item)
    if my_yolo is None:
        my_yolo = load_model(args.load)
    prd_boxes, prd_classes, prd_scores = predict(my_yolo, image)

    # Draw GT and PRD boxes on the image
    # ==================================
    img_np = np.array(image)
    gt_ann = draw_boxes_on_image(img_np.copy(), boxes=gt_boxes, classes=gt_classes, scores=gt_scores)
    #prd_ann = draw_boxes_on_image(img_np.copy(), boxes=prd_boxes, classes=prd_classes, scores=prd_scores, all_class_names=my_yolo.class_names)
    all_ann = draw_boxes_on_image(gt_ann.copy(), boxes=prd_boxes, classes=prd_classes, scores=prd_scores, all_class_names=my_yolo.class_names)

    # we measure one sided IoU  - only for the predicted boxes
    ious = [bb_iou_wrapper(prd_box, gt_boxes) for prd_box in prd_boxes]
    gt_ious = [bb_iou_wrapper(gt_box, prd_boxes) for gt_box in gt_boxes]
    nof_gt_detected = np.count_nonzero(np.array(gt_ious) > iou_thr )
    nof_gt_miss = len(gt_boxes) - nof_gt_detected

    avg_iou = np.mean(ious)

    stat = {
        'act_nof_boxes': len(gt_boxes),
        'prd_nof_boxes': len(prd_boxes),
        'prd_ious': ious,
        'gt_ious': gt_ious,
        'nof_gt_detected': nof_gt_detected,
        'nof_gt_missed': nof_gt_miss,
    }


    fig = plt.figure()
    plt.imshow(all_ann)
    plt.title(f"{len(prd_boxes)} predicted boxes w/ {avg_iou:.2f} mean /mu IoU")
    plt.axis('off')
    if show:
        plt.show()

    n_name = f"PRD-{len(prd_boxes)}-ACT-{len(gt_boxes)}-{item.filename.replace('/', '-')}"
    fig_path = f'{out_dir}/{n_name}'
    fig.savefig(fig_path, dpi=300)
    print(f"Saved figure to {fig_path} ::   prd box avg. iou {avg_iou:.2f}")
    return stat, item.filename


def compare_all_items(dataset: dl.entities.Dataset, my_yolo=None, model_name='ipm'):
    from glob import iglob
    import json
    from multiprocessing.pool import ThreadPool as Pool
    if my_yolo is None:
        my_yolo = load_model(args.load)
    out_dir = f'/tmp/ipm-compare-results/{model_name}'
    os.makedirs(out_dir, exist_ok=True)

    images_stats = {}
    for page in dataset.items.list(page_size=4):
        for item in page:
            stat, fname = compare_prd(item, my_yolo=my_yolo, out_dir=out_dir, show=False)
            images_stats[fname] = stat
        break

        # TODO: add pool based
       #  p = Pool(processes=2)
       #  kwds = {'my_yolo': my_yolo, 'out_dir': out_dir, 'show': False}
       #  jobs = [p.apply_async(compare_prd, args=(item, ), kwds=kwds) for item in page]
       #  p.close()
       #  p.join()
       #  for fname, stat in jobs:
       #      images_stats[fname] = stat


    # Calculate Global statistics - over all images
    nof_act_boxes = np.sum([stat['act_nof_boxes'] for stat in images_stats.values()])
    nof_prd_boxes = np.sum([stat['prd_nof_boxes'] for stat in images_stats.values()])
    prd_avg_iou = np.mean([iou for iou in stat['prd_ious'] for stat in images_stats.values()])
    act_avg_iou = np.mean([iou for iou in stat['gt_ious'] for stat in images_stats.values()])

    images_stats['total'] = {
        'act_avg_nof_boxes': int(nof_act_boxes),
        'prd_avg_nof_boxes': int(nof_prd_boxes),
        'prd_avg_iou': prd_avg_iou,
        'act_avg_iou': act_avg_iou
    }
    with open(f"{out_dir}/stats.json", 'w') as f:
        json.dump(images_stats, f, indent=2)


def draw_boxes_on_image(image: np.ndarray, boxes, classes, scores, all_class_names=None):
    import cv2
    font_size = 0.8  # np.floor(3e-4 * image.shape[1] + 0.5).astype('int32')
    thickness = int(image.shape[1] // 1e3)

    for i, c in reversed(list(enumerate(classes))):
        predicted_class = c if all_class_names is None else all_class_names[c]
        box = boxes[i]
        score = scores[i]

        if isinstance(score, str):  # For GT
            label = f'{predicted_class} {score}'
            clr = (0, 255, 0)
        else:
            label = f'{predicted_class} {score:.2f}'
            red = np.clip(255*score*3, 0, 255)  # dark red for low scores
            clr = (red, 0, 0)

        left, top, right, bottom = box  # NOTE: original was wrong order
        top = max(0, np.floor(top + 0.5).astype('int32'))
        left = max(0, np.floor(left + 0.5).astype('int32'))
        bottom = min(image.shape[1], np.floor(bottom + 0.5).astype('int32'))
        right = min(image.shape[0], np.floor(right + 0.5).astype('int32'))
        # print(f"{label} @  [{(top, left)} {(bottom, right)}]")

        image = cv2.rectangle(image, (top, left), (bottom, right), color=clr, thickness=2)
        image = cv2.putText(image, label, org=(top, left),
                            fontFace=cv2.FONT_HERSHEY_SIMPLEX, fontScale=font_size, thickness=thickness,
                            color=clr)
    # print(f"params used in drawing: thickness={thickness}, fontScale={font_size}")
    return image


def bb_iou_wrapper(boxA, boxes):
    """Wrapper that finds and returns the highest iou"""
    best = 0
    for boxB in boxes:
        iou = bb_intersection_over_union(boxA, boxB)
        if iou > best:
            best = iou
    return best

def bb_intersection_over_union(boxA, boxB):
    """box given with [x_min, y_min, x_max, y_max]"""
    # determine the (x, y)-coordinates of the intersection rectangle
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = abs(max((xB - xA, 0)) * max((yB - yA), 0))
    if interArea == 0:
        return 0
    # compute the area of both the prediction and ground-truth
    # rectangles
    boxAArea = abs((boxA[2] - boxA[0]) * (boxA[3] - boxA[1]))
    boxBArea = abs((boxB[2] - boxB[0]) * (boxB[3] - boxB[1]))

    # compute the intersection over union by taking the intersection
    # area and dividing it by the sum of prediction + ground-truth
    # areas - the interesection area
    iou = interArea / float(boxAArea + boxBArea - interArea)

    # return the intersection over union value
    return iou


@deprecation.deprecated(details="Moved to a seperate module for creating specific datasets")
def parse_annotation_single_item(item: dl.entities.Item, label_map):

    box_anns = [ann for ann in item.annotations.list() if ann.type == 'box']
    ann_format = f'{item.filename[1:]}'  # remove the '/' at begining
    # Box format: x_min,y_min,x_max,y_max,class_id (no space).
    for ann in box_anns:
        x_min, y_min = int(ann.x[0]), int(ann.y[0])
        x_max, y_max = int(ann.x[1]), int(ann.y[1])
        c_id = label_map.get(ann.label, 0) # if ann.label not found ==> defaults to index 0
        ann_format += f" {x_min},{y_min},{x_max},{y_max},{c_id}"

    ann_format += "\n"
    return ann_format


@deprecation.deprecated(details="Moved to a seperate module for creating specific datasets")
def prepare_train_data(dataset_name, label_map=None):
    """
    Download the data from dlp and create the gt vectors of the labels
    """
    # TODO: split train-val - Currently part of the test function
    import os
    import datetime
    project = dl.projects.get('IPM Glue Trap')
    dataset = project.datasets.get(dataset_name)  # FIXME   - add as var
    dataset_path = f"/tmp/yolo3-{datetime.datetime.now().strftime('%F')}"
    annotation_path = f"{dataset_path}/train.txt"
    os.makedirs(dataset_path, exist_ok=True)
    if label_map is None:
        label_map = dataset.instance_map  # `name` : `id`: int
        label_map = {k: v-1 for k, v in label_map.items()}


    filters = dl.Filters(field='annotated', values=True)
    filters.add(field='annotated', values='discarded', operator='ne')  # filter out discarded items
    filters.add_join(field="type", values="box")  # make sure only items w/ box annotations
    # Download the DQL related to train
    dataset.items.download(filters=filters, local_path=dataset_path, annotation_options='json')

    with open(f"{dataset_path}/ipm_classes.txt", 'w') as f:
        for cls in label_map.keys():
            f.write(f"{cls}\n")

    with open(annotation_path, 'w') as f:
        # create a new file
        pass

    cnt = 0
    pages = dataset.items.list(filters=filters)
    for page in tqdm(pages, position=0, desc="page", ncols=100, total=pages.total_pages_count):  # dynamic_ncols=
        for item in tqdm(page, position=0, leave=True, ncols=80):
            cnt += item.annotations_count
            with open(annotation_path, 'a') as f:
                item_anns_fmt = parse_annotation_single_item(item, label_map=label_map)
                f.write(item_anns_fmt)

    print(f"Created new dataset for train @{datetime.datetime.now().strftime('%X %F')} - total boxes {cnt}"
          ":::  create the model object with DQL, ids and name")


def train_model():
    pass
    # augmentaion
    # filter data by labels
    # train is @ keras-yolo/train.py

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(
        description='ipm insects model creation and interface',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--load', help='path to hd5 file to load the keras model')
    parser.add_argument('--prepare', default=False, action='store', help='name of dataset to prepare')
    parser.add_argument('--image-path', help='path to specific local image ')
    parser.add_argument('--dataset', help='dlp dataset name. preforms the compare on the entire dataset (of single item-id : if given) ')
    parser.add_argument('--item-id', help='dlp item id - if passed preforms only a single item comparison')


    parser.add_argument('--dlp-image', nargs=2, help='path to specific local image (dlp_dir, image_path) with dlp format annotation ')  # FIXME: change to the remote version
    parser.add_argument('--compare',  action='store_true', help='if passed preforms compare to all dlp-image directory')
    parser.add_argument('--model-name', default='ipm', help='model tag (name) to use in dir structure')

    args = parser.parse_args()

    # TODO: split the file to data_preparation
    if args.prepare:
        hard_coded_map ={'insect': 0, 'moth':1}
        prepare_train_data(dataset_name=args.prepare, label_map=hard_coded_map)
    elif args.load is not None:
        try:
            my_yolo = load_model(model_path=args.load)
            print(f"load model {args.load}")
        except FileNotFoundError:
            my_yolo = load_model(model_path='/home/shefi/dlp_ws/keras-yolo3/ipm-insects/yolo3-2020-09-14/logs_000/trained_weights_final.h5')
            print("loaded default local modle")

    if args.image_path:
        try:
            image = Image.open(args.image_path)
        except FileNotFoundError:
            print(f"input {args.image_path} was not found - using default kids image")
            image = Image.open('/home/shefi/Pictures/kids_ds/IMG-20200711-WA0032.jpg')
        prd_ann = predict(my_yolo, image, show=True)

    # DLP Remote predictions
    # ======================
    if args.dataset is not None:
        dl.setenv('prod')
        project = dl.projects.get('IPM Glue Trap')
        dataset = project.datasets.get(args.dataset)

        if args.item_id is not None:
            item = dataset.items.get(item_id=args.item_id)
            stat, fname = compare_prd(item, my_yolo=my_yolo, out_dir='/tmp', show=True, iou_thr=0.8)
            print(f"{fname}: {stat}")

        else:  # preform compare on all the dataset
            compare_all_items(dataset=dataset, my_yolo=my_yolo, model_name=args.model_name)

    # Clean up
    if 'my_yolo' in locals():
        my_yolo.close_session()

