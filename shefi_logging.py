import time
import logging
import os
from logging.handlers import RotatingFileHandler

def get_script_logger(name=None, to_file=False, level='INFO') -> logging.Logger :
    logger = logging.getLogger(name=name)
    logger.setLevel('DEBUG')

    # Handlers
    # ========
    os.makedirs('/tmp/shefi-logs', exist_ok=True)
    fmt = logging.Formatter('%(name)-20s:%(levelname)-8s %(asctime)-s [%(filename)-s:%(lineno)-d]:: %(msg)s', datefmt='%X')

    # add Stream handler if it's empty
    if not any([isinstance(hd, logging.StreamHandler) for hd in logger.handlers]):
        hdl = logging.StreamHandler()
        hdl.setFormatter(fmt)
        hdl.setLevel(level=level.upper())
        logger.addHandler(hdlr=hdl)

    # File Handler
    if not any([isinstance(hd, RotatingFileHandler) for hd in logger.handlers]):
        if to_file:
            f_hdl = RotatingFileHandler(filename=f"/tmp/{os.getenv('USER')}-logs/{name}.log", backupCount=3, maxBytes=1024*1024*5)
            logger.addHandler(f_hdl)

    return logger

def set_stream_handler(logger: logging.Logger, level=None, fmt=None, datefmt=None):
    for hd in logger.handlers:
        if level is not None:
            hd.setLevel(level=level.upper())
        if fmt is not None or datefmt is not None:
            hd.setFormatter(logging.Formatter(fmt=fmt, datefmt=datefmt))


def format_logger_state(logger):
    return  f"""logger {logger.name!r}: 
    level: {logger.level}={logging.getLevelName(logger.level)}. 
    handlers: {[(hd.name, hd.__class__.__name__, hd.level) for hd in logger.handlers]} \t (name,class,level) """


def test_lg():
    print(f"logging available levels are: {list(logging._nameToLevel.keys())}")
    lg = get_script_logger('shefi-test')
    lg.debug("test msg: debug")
    lg.info("test msg: info")
    lg.error("test msg: error")
    print(format_logger_state(logger=lg))
    time.sleep(0.05)

    lg.setLevel('DEBUG')
    lg.handlers[0].setLevel('DEBUG')
    print("set new level")
    lg.debug("test msg: debug - new level")
    lg.info("test msg: info - new level")
    lg.error("test msg: error - new level")
    print(format_logger_state(logger=lg))

def test_lg_file():
    lg = get_script_logger('shefi-test2')
    lg = get_script_logger('shefi-test2')
    lg.error("test with 2 logger")
    print(format_logger_state(logger=lg))

def test_lg_change():
    import time
    lg = get_script_logger('shefi-test3')
    lg.info("test 3 before change")
    lg.critical(format_logger_state(lg))
    set_stream_handler(lg, level='warn')
    time.sleep(0.1)
    lg.info("test 3 after change")
    lg.critical(format_logger_state(lg))


if __name__ == '__main__':
    # run module tests
    print(f"\nTEST - 1\n{'-'*10}")
    test_lg()
    time.sleep(0.2)
    print(f"\nTEST - 2\n{'-'*10}")
    test_lg_file()
    time.sleep(0.2)
    print(f"\nTEST - 3\n{'-'*10}")
    test_lg_change()
